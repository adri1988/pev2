package GUI;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.math.plot.Plot2DPanel;

import OperadoresGeneticos.AlgoritmoGenetico;
import controlador.Observador;
import logica.cromosoma;

public class PanelGrafica extends JPanel implements Observador{
	private AlgoritmoGenetico control;
	private JLabel panelError;

	public PanelGrafica(AlgoritmoGenetico c){
		control = c;
		control.addObserver(this);
		init();
	}
	
	private void init(){
		this.setBorder(BorderFactory.createTitledBorder("Gr�fica"));
		setLayout(new BorderLayout());
		panelError=new JLabel("Datos no validos. Vuelva a probar.");
		add(panelError, BorderLayout.CENTER);
		panelError.setVisible(false);
		panelError.setHorizontalAlignment(SwingConstants.CENTER);
		panelError.setFont(new Font("Serief", Font.BOLD, 48));
	}
	
	public void insertarPlot(Plot2DPanel framePlot) {
        this.add(framePlot);
    }

	public void quitarPlot(){
		this.removeAll();		
		init();
	}
	
	@Override
	public void addPlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
	}

	@Override
	public void minmax(boolean b) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mostrarErrorDatos() {
		//mostramos un error de datos no validos
		this.panelError.setVisible(true);
	}

	@Override
	public void activarHeu(boolean b) {
		// TODO Auto-generated method stub
	}

	@Override
	public void pasarMejor(cromosoma elMejor) {
	
		
	}

	
}
