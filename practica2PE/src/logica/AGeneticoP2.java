package logica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import OperadoresGeneticos.MetodosCruce;
import OperadoresGeneticos.MetodosMutacion;
import OperadoresGeneticos.MetodosSeleccion;

public class AGeneticoP2 {

	public MetodosSeleccion seleccion;
	public MetodosCruce cruces;
	public MetodosMutacion mutaciones;

	@Override

	public String toString() {
		String resultado = "";
		for (int i=0;i<tam_pob;i++)
			resultado += pob[i].toString(); 
		resultado = resultado + "El mejor: " + elMejor.toString() + "Posicion del mejor: " + pos_mejor + '\n'+ '\n'+ '\n';
		return resultado; 
	}

	cromosoma[] pob; // poblaciÃ³n
	int tam_pob; // tamaÃ±o poblaciÃ³n
	int num_max_gen; // nÃºmero mÃ¡ximo de generaciones
	cromosoma elMejor; // mejor individuo
	cromosoma[] elMejorLocal;
	int pos_mejor; // posiciÃ³n del mejor cromosoma
	double prob_cruce; // probabilidad de cruce
	double prob_mut; // probabilidad de mutaciÃ³n	
	public int gen_actual=0;	
	ArrayList<cromosoma> elite;
	public final int nOXPP=8;


	public AGeneticoP2(){}

	public void inicializa(int tam_pob2, int num_max_gen2, double prob_cruce2, double prob_mutacion) {

		this.tam_pob=tam_pob2;
		this.num_max_gen=num_max_gen2;	
		this.prob_cruce=prob_cruce2;
		this.prob_mut=prob_mutacion;		
		elMejor = new cromosoma(); 
		elMejor.inicializaCromosoma();
		elMejor.setAptitud(Double.MAX_VALUE);
		elMejorLocal = new cromosoma[num_max_gen+1];
		pob = new cromosoma[tam_pob]; // creamos el Array de cromosomas		
		for (int j = 0; j < tam_pob; j++) {
			pob[j] = new cromosoma();
			pob[j].inicializaCromosoma();
			pob[j].setAptitud( pob[j].evalua());
		}	
		gen_actual=0;
		seleccion = new MetodosSeleccion();
		cruces = new MetodosCruce(nOXPP);
		mutaciones = new MetodosMutacion();
	}

	public void evaluarPoblacion(){//Evaluamos la población buscando el mínimo siempre
		double punt_acu = 0; // puntuaciÃ³n acumulada
		double aptitud_mejor = Double.MAX_VALUE; // mejor aptitud
		double sumaptitud = 0; // suma de la aptitud	
		elMejorLocal[gen_actual] = new cromosoma();
		for(int i=0;i<tam_pob;i++){
			sumaptitud = sumaptitud + pob[i].getAptitud();
			if (pob[i].getAptitud() < aptitud_mejor){
				pos_mejor = i;
				aptitud_mejor = pob[i].getAptitud();
				elMejorLocal[gen_actual] = new cromosoma(pob[i]);
			}
		}		 
		//Rellenamos las puntuaciones y puntuaciones acumuladas
		for (int i=0; i< tam_pob ; i++) {

			pob[i].setPuntuacion(pob[i].getAptitud() /sumaptitud );
			pob[i].setPunt_acum( pob[i].getPuntuacion() + punt_acu); 
			punt_acu = punt_acu + pob[i].getPuntuacion();
		}		

		if(aptitud_mejor<elMejor.getAptitud())
			elMejor = new cromosoma(pob[pos_mejor]);

	}	


	public boolean terminado(){					
		return gen_actual == num_max_gen;
	}


	public void reproduccion(int tipo){
		/*
		 * Aquí implementamos los distintos tipos de reproducción
		 *  PMX, OX, Variantes de OX, Ciclos (CX), Recombinación de	rutas (ERX), Codificación Ordinal, y algún método propio.
		 *  tipo = 1 -> PMX
		 *  tipo = 2 -> OX
		 *  tipo = 3 -> Variante de OX
		 *  tipo = 4 -> Ciclos (CX)
		 *  tipo = 5 -> Recombinación de rutas (ERX)
		 *  tipo = 6 -> Codificación Ordinal		 *  
		 * */

		//seleccionados para reproducir
		int[] sel_cruce = new int[tam_pob];
		//contador seleccionados
		int num_sel_cruce = 0;		
		double prob;

		//Se eligen los individuos a cruzar
		for (int i=0;i<tam_pob;i++){
			//se generan tam_pob nÃºmeros aleatorios en [0 1)
			prob = 	Math.random();
			//se eligen los individuos de las posiciones i si prob < prob_cruce
			if (prob < prob_cruce ){
				sel_cruce[num_sel_cruce] = i;
				num_sel_cruce++;
			}
		}

		// el numero de seleccionados se hace par

		if ((num_sel_cruce%2)==1)
			num_sel_cruce--;			

		// se cruzan los individuos elegidos en un punto al azar

		for (int i=0;i<num_sel_cruce; i=i+2){
			cromosoma hijo1 = new cromosoma(pob[sel_cruce[i]]);
			cromosoma hijo2 = new cromosoma(pob[sel_cruce[i+1]]);
			switch (tipo){
			case 1:
				cruces.crucePMX(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2);
				break;
			case 2:
				cruces.cruceOX(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2);					
				break;
			case 3:
				cruces.cruceOXPP(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2);					
				break;
			case 4:
				cruces.crucePorCiclos(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2);
				break;
			case 5:
				cruces.cruceRecombinacionDeRutas(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2);
				break;
			case 6:
				cruces.cruceCodificacionOrdinal(pob[sel_cruce[i]], pob[sel_cruce[i+1]],hijo1, hijo2); 

				break;				
			default: 
				break;
			}	
			if ((compruebaHijo(hijo1) || compruebaHijo(hijo2))==false )
				System.out.println("Hijos mal!!");
			// los nuevos individuos sustituyen a sus progenitores
			pob[sel_cruce[i]] =  hijo1;
			pob[sel_cruce[i+1]] = hijo2;
		}	


	}

	public void mutacion(int tipo, int numHeu){  // Volver a implementar
		/*
		 * tipo=0 Inserción
		 * tipo=1 Intercambio
		 * tipo=2 Inversión
		 * tipo=3 Heurística
		 * tipo=4 por definir
		 * 
		 * */

		switch (tipo){
		case 0: 
			pob = mutaciones.insercion(tam_pob, pob, prob_mut);
			break;
		case 1: 
			pob = mutaciones.intercambio(tam_pob, pob, prob_mut);
			break;
		case 2: 
			pob = mutaciones.inversion(tam_pob, pob, prob_mut);
			break;
		case 3: 
			pob = mutaciones.heuristica(numHeu, tam_pob, pob, prob_mut);
			break;
		case 4: 
			break;
		default: break;
		}		
	}


	int randomWithRange(int min, int max){
		int range = (max - min) + 1;     
		return (int)(Math.random() * range) + min;
	}

	private boolean compruebaHijo (cromosoma hijo){
		boolean correcto=true;

		for (int i=0;i<hijo.longitudTotal-1;i++)
			for(int j=i+1;j<hijo.longitudTotal;j++)
				if (hijo.getGenes(i)==hijo.getGenes(j)){
					return false;

				}

		return correcto;
	}
	public cromosoma[] getPob() {
		return pob;
	}
	public cromosoma getPob(int i) {
		return pob[i];
	}


	public int getTam_pob() {
		return tam_pob;
	}

	public void setTam_pob(int tam_pob) {
		this.tam_pob = tam_pob;
	}

	public int getNum_max_gen() {
		return num_max_gen;
	}

	public void setNum_max_gen(int num_max_gen) {
		this.num_max_gen = num_max_gen;
	}

	public cromosoma getElMejor() {
		return elMejor;
	}

	public void setElMejor(cromosoma elMejor) {
		this.elMejor = elMejor;
	}

	public int getPos_mejor() {
		return pos_mejor;
	}

	public void setPos_mejor(int pos_mejor) {
		this.pos_mejor = pos_mejor;
	}

	public double getProb_cruce() {
		return prob_cruce;
	}

	public void setProb_cruce(double prob_cruce) {
		this.prob_cruce = prob_cruce;
	}

	public double getProb_mut() {
		return prob_mut;
	}

	public void setProb_mut(double prob_mut) {
		this.prob_mut = prob_mut;
	}



	public cromosoma[] getElMejorLocal() {
		return elMejorLocal;
	}

	public void setElMejorLocal(cromosoma[] elMejorLocal) {
		this.elMejorLocal = elMejorLocal;
	}

	public int getGen_actual() {
		return gen_actual;
	}

	public void setGen_actual(int gen_actual) {
		this.gen_actual = gen_actual;
	}
	public void setPob(cromosoma[] pob) {
		this.pob = pob;
	}

	//Elitismo
	public void separaMejores(int tamElite) {
		ArrayList<cromosoma> auxiliar = new ArrayList<cromosoma>();
		elite = new ArrayList<cromosoma>();
		for (int i=0;i<tam_pob;i++)
			auxiliar.add(pob[i]);
		for (int j=0;j<tamElite;j++){
			int mejor = sacaIndiceDelMejor(auxiliar);
			elite.add(auxiliar.get(mejor));
			auxiliar.remove(mejor);
			tam_pob--;
		}
		pob=auxiliar.toArray( new cromosoma[tam_pob]);
	}

	private int sacaIndiceDelMejor(ArrayList<cromosoma> auxiliar) {
		double mejor=auxiliar.get(0).getAptitud();
		int indiceMejor=0;

		for (int i=1;i<tam_pob;i++)			
			if (auxiliar.get(i).getAptitud()<mejor){
				mejor=auxiliar.get(i).getAptitud();
				indiceMejor=i;	
			}			
		return indiceMejor;
	}

	public void incluyeMejores(int tamElite) {
		cromosoma[] auxiliar = new cromosoma[tam_pob+tamElite];
		for (int i=0;i<tam_pob;i++){
			auxiliar[i] = new cromosoma(pob[i]);
		}
		for (int i=tam_pob;i<tamElite+tam_pob;i++){
			auxiliar[i]= new cromosoma(elite.get(i-tam_pob));
		}
		pob = auxiliar;
		tam_pob = tam_pob+tamElite;
	}

	public void elegirSeleccion(int i) {
		/*Seleccion = 1  Ruleta
		 * Seleccion=2   TorneoDeterministico
		 * Seleccion = 3 truncamiento
		 * Seleccion=4   Universal Estocastico
		 * Seleccion=5 Restos
		 * Seleccion=6 Ranking
		 */

		switch (i){
		case 1:
			pob = seleccion.seleccionRuleta(tam_pob, pob);
			break;
		case 2:				
			pob = seleccion.seleccionPorTorneoDeterministico(tam_pob, pob);
			break;
		case 3:
			pob = seleccion.seleccionTruncamiento(tam_pob, pob);
			break;
		case 4:
			pob = seleccion.seleccionUniversalEstocastica(tam_pob, pob);
			break;
		case 5:
			pob = seleccion.seleccionRestos(tam_pob, pob);
			break;
		case 6:
			pob = seleccion.seleccionRanking(tam_pob, pob);
			break;
		default: break;

		}



	}
}