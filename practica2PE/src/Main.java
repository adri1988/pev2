import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;
import org.math.plot.*;
import org.math.plot.plotObjects.Line;

import GUI.FrameAG;
import OperadoresGeneticos.AlgoritmoGenetico;

public class Main {
	

	public static void main(String[] args) {
		AlgoritmoGenetico c = new AlgoritmoGenetico();
		FrameAG frame = new FrameAG(c);
	}
	
	
	
}
