import logica.*;

public class mainSinGUI {


	public static void algoritmo(int tam_pob,int num_max_gen,double prob_cruce, double prob_mutacion, int seleccion, boolean elitismo, int reproduccion, int mutacion, int numHeu){

		int tamElite=0;
		if (elitismo){
			tamElite = (int) (tam_pob*0.02);
		}

		AGeneticoP2 p = new AGeneticoP2();
		p.inicializa(tam_pob,num_max_gen,prob_cruce,prob_mutacion);//En todas las funciones el par�metro para el numeroo de genes va a ser -1 exceptoo para la funci�on 4
		p.evaluarPoblacion();


		printFromMadrid(p.getElMejor());
		while (!p.terminado()){//Algoritmo gen�tico
			p.gen_actual++;
			if(elitismo)
				p.separaMejores(tamElite);

			ejecutaSeleccionEnBaseATipoDeSeleccion(p,seleccion);
			p.reproduccion(reproduccion);	
			p.mutacion(mutacion, numHeu);
			if(elitismo)
				p.incluyeMejores(tamElite);
			p.evaluarPoblacion();


			//printFromMadrid(p.getElMejor());
		}


		printFromMadrid(p.getElMejor());




	}

	public static void printFromMadrid(cromosoma mejor){

		for (int i=0;i<mejor.longitudTotal;i++){
			System.out.print(mejor.getFenotipo()[i] + ",");
		}
		System.out.print('\n');
		System.out.println(mejor.getAptitud());

	}

	private static void ejecutaSeleccionEnBaseATipoDeSeleccion(AGeneticoP2 p, int seleccion) {
		/*Seleccion = 1  Ruleta
		 * Seleccion=2   TorneoDeterministico
		 * Seleccion = 3 truncamiento
		 * Seleccion=4   Universal Estocastico
		 * 
		 * minMax = 1 si es de M�nimos o 0 si es de M�ximos
		 * */

		p.elegirSeleccion(seleccion);

	}

	public static void main(String[] args) {
		algoritmo(100,100,0.6, 0.1, 3, false, 5, 3, 2);

	}

}
