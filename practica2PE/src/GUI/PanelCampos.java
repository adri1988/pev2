package GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

import org.math.plot.Plot2DPanel;

import OperadoresGeneticos.AlgoritmoGenetico;
import controlador.Elitismo;
import controlador.Heuristica;
import controlador.Mutacion;
import controlador.Observador;
import controlador.Selecciones;
import controlador.TipoCruce;
import logica.cromosoma;

public class PanelCampos extends JPanel implements Observador{
	private AlgoritmoGenetico control;
	private int minMAX;
	
	private JPanel pCampos;
	private JPanel pBotones;
	
	//Campos:
	private JLabel etPoblacion;
	private JTextField textPoblacion;
	private JLabel etIter;
	private JTextField textIter;
	private JLabel etCruces;
	private JTextField textCruces;
	private JLabel etMutacion;
	private JTextField textMutacion;
	private JLabel etTipoCruce;
	private JComboBox<Object> boxTipoCruce;
	private JLabel etSelec;
	private JComboBox<Object> boxSelec;
	private JLabel etTipoMuta;
	private JComboBox<Object> boxTipoMuta;
	private JLabel etElite;
	private JComboBox<Object> boxElite;
	private JLabel etHeuristica;
	private JComboBox<Object> boxHeuristica;
	private JLabel maxIter;
	private JTextField maxIterTex;
	private JLabel etContra;
	private JRadioButton contra;
//______________meterle si o no de contractividad
	
	//Botones:
	private JButton lanza;
	private JButton relanza;
	private JButton elimina;
	
	public PanelCampos(AlgoritmoGenetico c){
		control = c;
		control.addObserver(this);
		minMAX=1;
		init();
	}
	
	private void init(){
		setLayout(new BorderLayout());
		
		panelC();
		add(pCampos, BorderLayout.NORTH);

		panelB();
		add(pBotones, BorderLayout.SOUTH);
		eventos();
		this.setBorder(BorderFactory.createTitledBorder("Campos"));
	}


	private void panelC() {
		pCampos = new JPanel();
		pCampos.setLayout(new GridLayout(2,10));
		
		etPoblacion=new JLabel("Poblacion: ");
		etPoblacion.setHorizontalAlignment(SwingConstants.RIGHT);
		textPoblacion=new JTextField(6);
		textPoblacion.setText("100");
		etIter=new JLabel("Generaciones: ");
		etIter.setHorizontalAlignment(SwingConstants.RIGHT);
		textIter=new JTextField(6);
		textIter.setText("300");
		etCruces=new JLabel("Prob de cruce: ");
		etCruces.setHorizontalAlignment(SwingConstants.RIGHT);
		textCruces=new JTextField(6);
		textCruces.setText("0.6");
		etTipoCruce=new JLabel("Reproduccion: ");
		etTipoCruce.setHorizontalAlignment(SwingConstants.RIGHT);
		boxTipoCruce=new JComboBox(TipoCruce.values());
		etMutacion=new JLabel("Prob de Mutacion: ");
		etMutacion.setHorizontalAlignment(SwingConstants.RIGHT);
		textMutacion=new JTextField(6);
		textMutacion.setText("0.1");
		etTipoMuta=new JLabel("Mutacion: ");
		etTipoMuta.setHorizontalAlignment(SwingConstants.RIGHT);
		boxTipoMuta=new JComboBox(Mutacion.values());
		etSelec= new JLabel("Seleccion: ");
		etSelec.setHorizontalAlignment(SwingConstants.RIGHT);
		boxSelec= new JComboBox(Selecciones.values());
		etElite=new JLabel("Elitismo: ");
		etElite.setHorizontalAlignment(SwingConstants.RIGHT);
		boxElite=new JComboBox(Elitismo.values()); 
		etHeuristica=new JLabel("Numero de genes: ");
		etHeuristica.setHorizontalAlignment(SwingConstants.RIGHT);
		boxHeuristica=new JComboBox(Heuristica.values());
		maxIter = new JLabel("Máximo Iteraciones: ");
		maxIter.setHorizontalAlignment(SwingConstants.LEFT);
		maxIterTex =new JTextField(6);
		maxIterTex.setText("1000");
		
		pCampos.add(etPoblacion);
		pCampos.add(textPoblacion);
		pCampos.add(etIter);
		pCampos.add(textIter);
		pCampos.add(etCruces);
		pCampos.add(textCruces);
		pCampos.add(etTipoCruce);
		pCampos.add(boxTipoCruce);
		pCampos.add(etMutacion);
		pCampos.add(textMutacion);
		pCampos.add(etTipoMuta);
		pCampos.add(boxTipoMuta);
		pCampos.add(etSelec);
		pCampos.add(boxSelec);		
		pCampos.add(etElite);
		pCampos.add(boxElite);		
		pCampos.add(maxIter);
		pCampos.add(maxIterTex);
		maxIter.setVisible(false);
		maxIterTex.setVisible(false);
		pCampos.add(etHeuristica);
		etHeuristica.setVisible(false);
		pCampos.add(boxHeuristica);
		boxHeuristica.setVisible(false);
	}
	
	private void panelB() {
		pBotones = new JPanel();
		lanza=new JButton("Lanzar AG");
		elimina=new JButton("Eliminar AG");
		pBotones.add(lanza);
		pBotones.add(elimina);
		etContra = new JLabel("Contractividad");
		contra = new JRadioButton();
		pBotones.add(etContra);
		pBotones.add(contra);
		
	}
	
	private void eventos(){
		lanza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double cruces=Double.parseDouble(textCruces.getText());
				int iter=Integer.parseInt(textIter.getText());
				int tam=Integer.parseInt(textPoblacion.getText());
				double muta=Double.parseDouble(textMutacion.getText());
				int seleccion = boxSelec.getSelectedIndex() + 1;
				int repro = boxTipoCruce.getSelectedIndex() + 1;
				boolean elitismo; 
				if(boxElite.getSelectedIndex()==0) elitismo=false;
				else elitismo=true;
				int tipoMutacion = boxTipoMuta.getSelectedIndex();
				int nHeu=2;
				if(boxTipoMuta.getSelectedItem().equals(Mutacion.Heuristica))
					nHeu = boxHeuristica.getSelectedIndex()+2; 
				int numMax = Integer.parseInt(maxIterTex.getText());
				
				if((cruces>=0)&&(cruces<=1)&&(muta>=0)&&(muta<=1)&&(iter>0)&&(tam>0)){
					control.algoritmo(tam, iter,cruces ,muta, seleccion, elitismo, repro, tipoMutacion, nHeu,numMax, contra.isSelected());					
				}else{
					control.datosErroneos();
				}
			}
		});
		contra.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(contra.isSelected()){
					maxIter.setVisible(true);
					maxIterTex.setVisible(true);
				}else{
					maxIter.setVisible(false);
					maxIterTex.setVisible(false);
				}
			}
		});
		boxTipoMuta.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Mutacion f = (Mutacion) boxTipoMuta.getSelectedItem();
				control.cambioMutacion(f);
			}
		});
		elimina.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//quita el panel del plot
				control.eliminaPlot();
				//eliminar tambien el frame generado con resultados
			}
		});
	}

	@Override
	public void addPlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void minmax(boolean b) {
		if(b) this.minMAX=1;
		else this.minMAX=0;
	}

	@Override
	public void activarHeu(boolean equals) {
		etHeuristica.setVisible(equals);
		boxHeuristica.setVisible(equals);
	}

	@Override
	public void mostrarErrorDatos() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pasarMejor(cromosoma elMejor) {
		// TODO Auto-generated method stub
		
	}
	
	
	//Fin de clase
}
