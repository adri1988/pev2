package GUI;

import javax.swing.*;

import org.math.plot.Plot2DPanel;

import OperadoresGeneticos.AlgoritmoGenetico;
import controlador.Observador;
import logica.cromosoma;

import java.awt.*;
import java.util.ArrayList;


public class FrameAG extends JFrame implements Observador{
	private Container panelPrincipal;
	private PanelCampos panelSup;
	private PanelGrafica panelInf;
	private ArrayList<Plot2DPanel> listaPlot;
	private AlgoritmoGenetico control;
	private JPanel panelResultado;
	

	public FrameAG(AlgoritmoGenetico c) throws HeadlessException {
		super("Practica 2 - Programacion Evolutiva");
		this.control = c;
		this.panelPrincipal = this.getContentPane();
		control.addObserver(this);
		init();
	}
	
	private void init(){
		this.setLayout(new BorderLayout());
		
		//panelSup
		panelSup=new PanelCampos(this.control);
		panelPrincipal.add(panelSup, BorderLayout.NORTH);
		//panelInf
		panelInf=new PanelGrafica(this.control);
		panelPrincipal.add(panelInf, BorderLayout.CENTER);
		//panelResultado
		this.panelResultado=new JPanel();
		panelResultado.setLayout(new BorderLayout());
		panelPrincipal.add(panelResultado, BorderLayout.SOUTH);
		
		this.setSize(1500,800);	
		this.setLocation(350, 100);
		this.setVisible(true);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void insertarPlot(Plot2DPanel plot){
		panelInf.insertarPlot(plot);
		System.out.println("plot insertado");
	}
	public void quitarPlot(){
		panelInf.quitarPlot();
		panelResultado.removeAll();
		System.out.println("plot eliminado");
	}

	@Override
	public void addPlot(Plot2DPanel plot) {
		this.insertarPlot(plot);
		this.setVisible(true);
	}

	@Override
	public void removePlot(Plot2DPanel plot) {
		this.quitarPlot();
	}

	@Override
	public void minmax(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarErrorDatos() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activarHeu(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pasarMejor(cromosoma elMejor) {
		//Mostramos el mejor cromosoma generado
		elMejor.evalua();
		double distancia = elMejor.getAptitud();
		String[] fenotipo = elMejor.getFenotipo();
		String ciudades="Recorrido: ";
		int posMadrid =0;
		for (int i=0; i<fenotipo.length-1; i++){
			if (elMejor.getGenes(i)==0){
				posMadrid=i;
				break;
			}				
		}
	
	
		for(int i=posMadrid; i<fenotipo.length +posMadrid -1; i++){
			ciudades+=fenotipo[i % fenotipo.length] + ", ";
			
		}
		
		
		/*for(int i=0; i<fenotipo.length-1; i++){
			ciudades+=fenotipo[i] + ", ";
		}
		ciudades+=fenotipo[fenotipo.length-1];*/
		
		JLabel dist = new JLabel("Distancia: " + (int)distancia + " km");
		JLabel reco = new JLabel(ciudades);
		panelResultado.add(dist, BorderLayout.NORTH);
		panelResultado.add(reco, BorderLayout.SOUTH);
		
		System.err.println(distancia + "km");
		System.err.println(ciudades);
				
	}
	
}
