package logica;

public  class cromosoma {
	
	public int[] genes; //cadena de bits (genotipo)
	String[] fenotipo; //fenotipo---Distancia total
	public double aptitud;//Distancia total
	double puntuacion; //puntuación relativa(aptitud/suma)
	public double punt_acum; //puntuación acumulada para selección
	public final int longitudTotal=28;
	//Ciudades dist;
	
	
	
	public cromosoma() {
		// TODO Auto-generated constructor stub
		fenotipo = new String[longitudTotal];
		genes = new int[longitudTotal];
	}
	public cromosoma(cromosoma hijo1) {
		this.aptitud = hijo1.getAptitud();
		this.fenotipo=hijo1.getFenotipo();
		this.punt_acum=hijo1.getPunt_acum();	
		copiaGenes(hijo1);
		
	}	

	private void copiaGenes(cromosoma hijo1){
		
		int cantidadGen=0;
		genes = new int[longitudTotal];		
			for (int j=0;j<longitudTotal;j++){
				this.genes[cantidadGen]=hijo1.getGenes(cantidadGen);
				cantidadGen++;
		}
	}
	
public void copiaGenes(Object[] g){
		
		int cantidadGen=0;
		genes = new int[longitudTotal];		
			for (int j=0;j<longitudTotal;j++){
				this.genes[j]=(int) g[j];
				cantidadGen++;
		}
	}


	public void fenotipo() {//el valor del individuo
	/*
	 * 
	 * Obtener Array de strings
	 * Suponemos que el array fenotipo ya tiene el array creado
	 * 
	 * */
		for (int i=0;i<longitudTotal;i++)
			fenotipo[i]=Ciudades.getciudad(genes[i]);
	
	} 
	
	public double evalua() {//adaptacion del individuo		
		fenotipo();
		aptitud =  f(genes);
		return aptitud;	
	}
	
	private int f(int[] genes) {
		/*
		 * Tenemos que devolver la distancia total recorrida por el individuo
		 * 
		 * */
		int totalAcumulado=0;
		for (int i=0;i<longitudTotal-1;i++)
			totalAcumulado+=Ciudades.obtenerDistancia(genes[i], genes[i+1]);
		totalAcumulado += Ciudades.obtenerDistancia(genes[longitudTotal-1], genes[0]);
		return totalAcumulado;
	}
	

	public void inicializaCromosoma() {
		/*
		 * Generar permutaciones de 28 elementos sin repeticion
		 * 
		 * Como la formula de las permutaciones es una locura, vamos a generar los elementos a mano
		 * 
		*/
		fenotipo = new String[longitudTotal];
		genes = new int[longitudTotal];
		boolean esta[] = new boolean[longitudTotal];
		int n = 0;
		
		for(int i=0; i<longitudTotal; i++){
			while(esta[n]){
				n = (int) ((Math.random() * longitudTotal));
			}
			genes[i]=n;
			esta[n]=true;			
		}
	}
	
	public String toString(){
		String resultado = "";
		for (int i=0;i<longitudTotal;i++)
			resultado += genes[i] +  " , ";
		return "Genes: " + resultado + " fenotipo: " + fenotipo + " Aptitud: " + aptitud + " puntuación: " + puntuacion + " Punt acumulada: " + punt_acum +'\n';
		
	}
	
	public int getGenes(int i) {
		return genes[i];
	}
	public void setGenes(int[] genes) {
		this.genes = genes;
	}
	public String[] getFenotipo() {
		return fenotipo;
	}
	
	public void setFenotipo(String[] fenotipo) {
		this.fenotipo = fenotipo;
	}		
	
	public void setGenEnPos(int pos, int valor){
		this.genes[pos]=valor;
	}
	public double getAptitud() {
		return aptitud;
	}
	public void setAptitud(double aptitud) {
		this.aptitud = aptitud;
	}
	public double getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(double d) {
		this.puntuacion = d;
	}
	public double getPunt_acum() {
		return punt_acum;
	}
	public void setPunt_acum(double d) {
		this.punt_acum =	d;
	}
	
	int randomWithRange(int min, int max)
	{
	   int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
	public int getlongitudTotal() {
		// TODO Auto-generated method stub
		return longitudTotal;
	}


}
