package OperadoresGeneticos;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import logica.cromosoma;

public class MetodosSeleccion {
	
	

	public MetodosSeleccion(){

	}

	public cromosoma[] seleccionRanking(int tam_pob, cromosoma[] pob){
		/* En la seleccion por ranking se ordenan los individuos por su fitness y se les asigna
		   un valor de 1 a tam_pob.
		   Ordenamos de peor a mejor, asignamos valores de 1 a tam_pob, obtenemos los
		   acumulados de cada individuo, rellenamos la nueva poblacion.
		*/
		double prob; // probabilidad de seleccion
		cromosoma[] nueva_pob= new cromosoma[tam_pob]; 
		cromosoma[] ordenados= pob;
		
		for(int i=0;i<(tam_pob-1);i++){//ordenamos los cromosomas por aptitud.(mayor distancia a menor)
            for(int j=i+1;j<tam_pob;j++){
                if(ordenados[i].aptitud<ordenados[j].aptitud){
                    //Intercambiamos valores
                    cromosoma aux=ordenados[i];
                    ordenados[i]=ordenados[j];
                    ordenados[j]=aux;
                }
            }
        }
		int[] acumulados = new int[tam_pob];
		acumulados[0]=1;
		for(int i=1; i<tam_pob; i++){//asignamos un valor acumulado dependiendo del ranking
			acumulados[i] = acumulados[i-1] + i+1;
		}
		for(int i=0; i<tam_pob; i++){
			prob = randomWithRange(0,acumulados[acumulados.length-1]);
			if(prob>=0 && prob<=acumulados[0]){
				nueva_pob[i]=new cromosoma(ordenados[0]);
			}else{
				for(int j=1; j<tam_pob; j++){
					if(prob<=acumulados[j] && prob>acumulados[j-1]){//encontramos el individuo seleccionado y lo insertamos en la nueva pob		
						nueva_pob[i]=new cromosoma(ordenados[j]);
						j=tam_pob;
					}
				}
			}
		}		
		
		return nueva_pob;
		
	}
	public cromosoma[] seleccionRuleta(int tam_pob, cromosoma[] pob){
		int[] sel_super = new int[tam_pob];//seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posiciÃ³n del superviviente
		cromosoma[] nueva_pob= new cromosoma[tam_pob]; 
		for (int i=0;i<tam_pob;i++){
			prob = 	Math.random();
			pos_super=0;
			while ((pos_super<tam_pob) && (prob>pob[pos_super].punt_acum)){
				sel_super[i] = pos_super;
				pos_super++;
			}
		}		
		for (int i=0;i<tam_pob;i++){//genero la población intermedia
			nueva_pob[i] =  new cromosoma(pob[sel_super[i]]);
		}
		return nueva_pob;
	}
	
	public cromosoma[] seleccionRestos(int tam_pob, cromosoma[] pob){
		// con la puntuacion pi·K y se eligen esas copias de un individuo.
		//Si faltan huecos libres en la nueva pob, se rellenan por ruleta.
		double k = (tam_pob)/5; //k es 1/5 de la poblacion elegida
		double[] probs = new double[tam_pob];
		cromosoma[] nuevPob = new cromosoma[tam_pob];
		double[] probAcumulada = new double[tam_pob];

		int sumaApt=0;
		for(int i=0; i<tam_pob; i++){
			sumaApt+=pob[i].getAptitud();
			pob[i].setPunt_acum(sumaApt);
		}
		double probAcum = 0;
		for(int i=0; i<tam_pob; i++){//Guardamos en el array probs la probabilidad de cada individuo
			probs[i]=1-(pob[i].getAptitud()/sumaApt);
			probAcumulada[i]=probs[i] + probAcum;
			probAcum=probAcumulada[i];
		}
		double ncopias;
		int i=0;//pos de pob original
		int n=0;//pos de pob nueva
		while(i<tam_pob && n<tam_pob){
			ncopias = probs[i]*k;
			if(ncopias>=1){
				for(int j=0; j<((int)ncopias)&&(n<tam_pob); j++){
					nuevPob[n] = new cromosoma(pob[i]);
					n++;
				}
			}
			i++;
		}
		double r=0;
		while(n<tam_pob){//hay que rellenar los huecos libres, si los hay. Por ruleta.
			r = Math.random();
			if(r>=0 && r<=probAcumulada[0]){
				nuevPob[n] = new cromosoma(pob[0]);
			}else{
				for(int x=1; x<tam_pob; x++){
					if(r<=probAcumulada[x] && r>probAcumulada[x-1]){
						nuevPob[n] = new cromosoma(pob[x]);
					}
				}
			}
			n++;
		}
		return nuevPob;
	}
	
	
	
	public cromosoma[] seleccionTruncamiento(int tam_pob, cromosoma[] pob){//es de mínimos siempre
		
		int[] sel_super = new int[tam_pob];//seleccionados para sobrevivir
		int[] indices = new int[tam_pob];//indices 
		cromosoma[] nueva_pob= new cromosoma[tam_pob]; 
		HashMap<Integer, Double> list =new HashMap<>();
		for (int i=0;i<tam_pob;i++){
			list.put(i,  pob[i].getAptitud());			
		}
		list=(HashMap<Integer, Double>) sortByValue(list);
		 double trunc = 0.2;//usamos el 20% para el umbral
		 int cantidadAptos = (int) (tam_pob*trunc);
		 int repeticiones = tam_pob/cantidadAptos;
		 
		 Iterator<Entry<Integer, Double>> it = list.entrySet().iterator();
		 int p=0;
		 while (it.hasNext()){//Al salir de este bucle, en el array indices, voy a tener los indices de los individuos ordenados de menor a mayor
			 
			Entry<Integer, Double> pair = it.next();
			 int indice  = (int) pair.getKey();
			 indices[p]=indice;
			 p++;
		 }
		 int desplazamiento=0;
		 for (int i=0;i<cantidadAptos;i++){	
			 int indice; 			
			 indice = indices[i];
			 for (int j=0;j<repeticiones;j++){	
				 sel_super[desplazamiento]=indice;
				 desplazamiento++;
			 }
		 }
			 
		 for (int i=0;i<tam_pob;i++){//genero la población intermedia
			nueva_pob[i] =  new cromosoma(pob[sel_super[i]]);
		 }
		 return nueva_pob;
		
	}
	private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map )
	{
	    List<Map.Entry<K, V>> list =new LinkedList<>( map.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<K, V>>()
	    {
	        @Override
	        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
	        {
	            return (o1.getValue()).compareTo( o2.getValue() );
	        }
	    } );

	    Map<K, V> result = new LinkedHashMap<>();
	    for (Map.Entry<K, V> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}

	public cromosoma[] seleccionUniversalEstocastica(int tam_pob, cromosoma[] pob){
		/*
		 * Similar al muestreo proporcional pero ahora se genera un único número aleatorio simple r
		 * y a partir de él se calculan los restantes. 
		 * �?�Los individuos se mapean en segmentos continuos cuyo tamaño es el de su aptitud. 
		 * �?�Se colocan tantas marcas espaciadas por igual como individuos queremos seleccionar (N) 
		 * �?�La distancia entre las marcas es 1/N 
		 * �?�La posición de la primera marca se obtiene a partir de un número aleatorio entre 0y 1/N. 		 * 
		 * 
		 * */
		int[] sel_super = new int[tam_pob+1];//seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posiciÃ³n del superviviente
		cromosoma[] nueva_pob= new cromosoma[tam_pob];
		double r = 	Math.random();//elemento aleatorio único que voy a generar
		for (int i=1;i<=tam_pob;i++){//tengo que aplicar la formula Aj=(a+j-1)/k donde a es el valor aleatorio, j el j-ésimo elemento y la K tam_pob			
			pos_super=0;
			prob = 	Math.random();
			float Ai = (float) ((r+i-1)/tam_pob);//calculo la posición del i-ésimo elemento 
			while ((prob>Ai) && (pos_super<tam_pob)){
				sel_super[i] = pos_super;
				pos_super++;
			}
		}		
		for (int i=0;i<tam_pob;i++){//genero la población intermedia
			nueva_pob[i] =  new cromosoma(pob[sel_super[i]]);
		}
		return nueva_pob;
		
		
	}
		
	public cromosoma[] seleccionPorTorneoDeterministico(int tam_pob, cromosoma[] pob){
		/*
		 * Determinística 
		 * �?�Cada elemento de la muestra se toma eligiendo el mejor de los individuos de un conjunto 
		 * de zelementos(2 ó 3) tomados al azar de la población base. 
		 * 
		 * �?�El proceso se repite kveces hasta completar la muestra. 
		 * 
		 * �?�Probabilística
		 *  �?�Se diferencia en el paso de selección del ganador del torneo. 
		 * En vez de escoger siempre el mejor se genera un número aleatorio del intervalo [0..1], 
		 * si es mayor que un parámetro p 
		 * (fijado para todo el proceso evolutivo)
		 *  se escoge el individuo más alto y en caso contrario el menos apto. 
		 *  Generalmente p toma valores en el rango (0.5,1)
		 * */
		cromosoma[] nueva_pob= new cromosoma[tam_pob];
		for(int i=0;i<tam_pob;i++){
		//tengo que obtener una muestra de tamaño 2 y asegurarme que no son iguales
		int muestra1 = randomWithRange(0,tam_pob-1); // probabilidad de seleccion
		int muestra2 = randomWithRange(0,tam_pob-1); // probabilidad de seleccion
		while (muestra1==muestra2){
			 muestra1 = randomWithRange(0,tam_pob-1); // probabilidad de seleccion
			 muestra2 = randomWithRange(0,tam_pob-1); // probabilidad de seleccion
		}
		//Al salir del bucle me aseguro que muestra1!=Muestra2
		
		//Obtengo el fitness de cada muestra de la población
		double muestra1Fitness = pob[muestra1].getAptitud();
		double muestra2Fitness = pob[muestra2].getAptitud();
		
		//Las comparo
		int pos_super;// posición del superviviente
		if (muestra1Fitness<muestra2Fitness){
			pos_super=muestra1;//es mejor la muestra 1
		}
		else{
			pos_super=muestra2;//es mejor la muestra 2
		}
		nueva_pob[i] = new cromosoma(pob[pos_super]);
		}
		
		
	
		return nueva_pob;
	}
	
	int randomWithRange(int min, int max){
		   int range = (max - min) + 1;     
		   return (int)(Math.random() * range) + min;
		}

	
	
}
