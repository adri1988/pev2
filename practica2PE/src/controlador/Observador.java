package controlador;

import org.math.plot.Plot2DPanel;

import logica.cromosoma;

public interface Observador {

	void addPlot(Plot2DPanel plot);

	void removePlot(Plot2DPanel plot);

	void minmax(boolean b);

	void mostrarErrorDatos();

	void activarHeu(boolean b);

	void pasarMejor(cromosoma elMejor);
	
}
