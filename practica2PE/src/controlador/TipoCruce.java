package controlador;

public enum TipoCruce {
	PMX("Cruce por emparejamiento parcial"){},
	OX("Cruce por orden"){},
	OXPP("Cruce por orden con posiciones prioritarias"){},
	CX("Cruce por ciclos"),
	Variante_ERX("Cruce por recombinacion de rutas"),
	ORDINAL("Codificacion ordinal"),
//	
//	Monopunto("Cruce monopunto o simple"){},
//	Discreto("Cruce discreto uniforme"){},
	//AritmeticoCompleto("Cruce aritmetico completo")
	;
	
	private String desc;

	private TipoCruce(String d){
		this.desc = d;
	}
	
	public String getDesc(){
		return desc;
	}
	
}
