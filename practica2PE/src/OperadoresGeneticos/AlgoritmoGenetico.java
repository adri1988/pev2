package OperadoresGeneticos;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import org.math.plot.Plot2DPanel;
import org.math.plot.PlotPanel;

import controlador.Mutacion;
import controlador.Observador;
import logica.AGeneticoP2;


public class AlgoritmoGenetico {
	
	//private Funciones funcionActual;
	private ArrayList<Observador> observer;
	private Plot2DPanel plotActual;
	
	public AlgoritmoGenetico(){
	//	funcionActual = Funciones.Funcion1;
		observer = new ArrayList<Observador>();
	}
	
	public void addObserver(Observador v) {
		this.observer.add(v);	
	}
	
	public void algoritmo(int tam_pob,int num_max_gen,double prob_cruce, double prob_mutacion, int seleccion, boolean elitismo, int reproduccion, int mutacion, int numHeu,int max, boolean contractividad){
		
		double [] generaciones = new double[num_max_gen+1];		
		double [] aptitudesDelMejorGlobal = new double[num_max_gen+1];
		double [] aptitudesMejoresLocales = new double[num_max_gen+1];
		double [] mediasDeAptitudes = new double[num_max_gen+1];
		double [][] fenotipos;
		int tamElite=0;
		
		for (int i=0; i<=num_max_gen;i++){
			generaciones[i]=i; 
	     }  
		if (elitismo){
			 tamElite = (int) (tam_pob*0.02);
		}
		
		AGeneticoP2 p = new AGeneticoP2();
		p.inicializa(tam_pob,num_max_gen,prob_cruce,prob_mutacion);//En todas las funciones el par�metro para el numeroo de genes va a ser -1 exceptoo para la funci�on 4
		p.evaluarPoblacion();		
		mediasDeAptitudes[0]=calculaMediaDePoblacion(p);
		aptitudesDelMejorGlobal[0]=p.getElMejor().getAptitud();//Almaceno el mejor de la poblaci�on inicial	
		aptitudesMejoresLocales[0]=p.getElMejorLocal()[0].getAptitud();
	
		
		
		
		p.gen_actual++;		
		int t=0;
		
		while (!p.terminado() && t<=max){//Algoritmo gen�tico
			
			if(elitismo)
				p.separaMejores(tamElite);
			
			ejecutaSeleccionEnBaseATipoDeSeleccion(p,seleccion);
			p.reproduccion(reproduccion);	
			p.mutacion(mutacion, numHeu);
			if(elitismo)
				p.incluyeMejores(tamElite);
			p.evaluarPoblacion();
			mediasDeAptitudes[p.gen_actual]=calculaMediaDePoblacion(p);
			aptitudesDelMejorGlobal[p.gen_actual]=p.getElMejor().getAptitud();//Almaceno el mejor de la generaci�n actual		
			aptitudesMejoresLocales[p.gen_actual]=p.getElMejorLocal()[p.gen_actual].getAptitud();
			
			if(contractividad){	
				if (mediasDeAptitudes[p.gen_actual]<mediasDeAptitudes[p.gen_actual-1]){
					p.gen_actual++;
				}
				t++;
			}else{
				p.gen_actual++;
			}
			
			
		}

	

		Plot2DPanel plot = new Plot2DPanel();		
		plot.addLegend("SOUTH");
		plot.addLinePlot("Mejor Local", generaciones, aptitudesMejoresLocales);		
		plot.addLinePlot("Mejor Global", generaciones, aptitudesDelMejorGlobal);		
		plot.addLinePlot("Media de la Generacion", generaciones, mediasDeAptitudes);
		
		this.plotActual=plot;
		for(Observador o:observer){
			o.removePlot(null);
			o.addPlot(plot);
			o.pasarMejor(p.getElMejor());
		}
		
	}	
	

	private static double calculaMediaDePoblacion(AGeneticoP2 p) {
		int tam=p.getTam_pob();
		double media =0;
		for (int i=0;i<tam;i++){
			media+=p.getPob(i).getAptitud();
		}
		media = media/tam;
		return media;
	}


	private static void ejecutaSeleccionEnBaseATipoDeSeleccion(AGeneticoP2 p, int seleccion) {
		/*Seleccion = 1  Ruleta
		 * Seleccion=2   TorneoDeterministico
		 * Seleccion = 3 truncamiento
		 * Seleccion=4   Universal Estocastico
		 * Seleccion=5 Restos
		 * Seleccion=6 Ranking
		 * 
		 * minMax = 1 si es de M�nimos o 0 si es de M�ximos
		 * */
		
		p.elegirSeleccion(seleccion);
		
	}

	public void cambioMutacion(Mutacion f) {
		if(f.equals(Mutacion.Heuristica)){
			for(Observador o:observer){
				o.activarHeu(true);
			}
		}else{
			for(Observador o:observer){
				o.activarHeu(false);
			}
		}
		
	}

	public void eliminaPlot() {
		for(Observador o:observer){
			o.removePlot(plotActual);
		}
	}

	public void datosErroneos() {
		for(Observador o:observer){
			o.mostrarErrorDatos();
		}		
	}
	
	
	
}



	
	

