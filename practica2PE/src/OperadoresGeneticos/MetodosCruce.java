package OperadoresGeneticos;

import java.util.ArrayList;

import logica.cromosoma;

public class MetodosCruce {

	private int nOXPP;
	
	
	public MetodosCruce(int OXPP){
		this.nOXPP=OXPP;
	}
	
	public void cruceRecombinacionDeRutas(cromosoma padre1, cromosoma padre2, cromosoma hijo1,
			cromosoma hijo2) {
	   /* tenemos que cacular las adyacencias de cada hijo, como todas las ciudades estan conectadas unas con otras, 
	    * las adyancencias tendrás el mismo número de elementos, por lo que será mayormente aleatorio
	    */
		
		
		realizaERX(padre1,hijo2);
		realizaERX(padre2,hijo1);
		
		hijo2.evalua();
		hijo1.evalua();
		
		
	}
	
private void realizaERX(cromosoma padre1, cromosoma hijo1){
	
	
	ArrayList<Integer> adP1 = toArrayList(padre1.genes);		
	ArrayList<Integer> vecinos = new ArrayList<Integer>();
	
	ArrayList<Integer> hijo1Genes = new ArrayList<Integer>();
	
	for (int i=0;i<padre1.longitudTotal;i++){//todos los vecinos van a ser iguales
		vecinos.add(i);
	}
	ArrayList<ArrayList<Integer>> matrizDeAdy = new ArrayList<ArrayList<Integer>>();
	for (int i=0;i<padre1.longitudTotal;i++){
		ArrayList<Integer> vecinoI = (ArrayList<Integer>) vecinos.clone();
		vecinoI.remove(adP1.get(i));
		matrizDeAdy.add(vecinoI);
	}
	
	//en adP1 tengo los vertices y en matrizDeAdy tengo todos los vecinos de adP1
	
	//genero una posicion aleatorio y esa será mi ciudad inicial
	int ciudadActual = randomWithRange(1,padre1.longitudTotal-1);
	boolean[] esta = new boolean[padre1.longitudTotal];
	for (int i=0;i<padre1.longitudTotal;i++) //inicializamos el array Esta a false
		esta[i]=false;
	int contador =0;
	while (contador<padre1.longitudTotal){
		hijo1Genes.add(ciudadActual);
		esta[ciudadActual]=true;//añadimos la ciudad actual
		if (hijo1Genes.size()==padre1.longitudTotal)
			break;
		
		//tenemos que comprobar quen de los vertices tiene menos hijos, si es empate, se escoge aleatoriamente
		//como todos los padres tienen longitudIgual, escogemos la primera libre 
		int proximaCiudad = dameCiudadLibreDe(vecinos,esta);	
		ciudadActual=proximaCiudad;			
		contador++;
	}
	
	hijo1.copiaGenes(hijo1Genes.toArray());
}

	private int dameCiudadLibreDe(ArrayList<Integer> vecinos, boolean[] esta) {
	int ciudad=0;
		ciudad = randomWithRange(0,esta.length-1);
		boolean valido=false;
		while (!valido){
			if (!esta[ciudad]){
				ciudad = vecinos.get(ciudad);
				break;
			}
			else 
				ciudad = randomWithRange(0,esta.length-1);
		}
		
	return ciudad;
}


	
	public void crucePorCiclos(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2) {
				
		ArrayList<Integer> p1 = toArrayList(padre1.genes);
		ArrayList<Integer> p2 = toArrayList(padre2.genes);
		
		ArrayList<Integer> cicloHijo1 = encuentraCiclo(p1,p2);
		ArrayList<Integer> cicloHijo2 = encuentraCiclo(p2,p1);
		
		//Después de tener los ciclos, ahora tenemoos que rellenar los huecos restantes
		
		ArrayList<Integer> h1 = new ArrayList<Integer>();
		ArrayList<Integer> h2 = new ArrayList<Integer>();
		
		for (int i=0;i<padre1.longitudTotal;i++){//rellenamos el array auxiliar de -1
			h1.add(-1);
			h2.add(-1);
		}
		
		for (int i=0;i<cicloHijo1.size();i++) //Rellenamos el hijo 1 y 2 con los ciclos correspondientes
			h1.set(p1.indexOf(cicloHijo1.get(i)), cicloHijo1.get(i));
		
		for (int i=0;i<cicloHijo2.size();i++)
			h2.set(p2.indexOf(cicloHijo2.get(i)), cicloHijo2.get(i));
		
		//Ahora el hijo 1 se rellena con los valores restantes del padre 2
		
		for (int i=0;i<p1.size();i++){
			int p = h1.get(i);
			boolean cond = cicloHijo1.contains(p);
			
			if (!cond){
				h1.set(i,p2.get(i));
				
			}
		}
		
		//ahora el hijo2 se rellena con los valores restantes del padre 1
		
		for (int i=0;i<p2.size();i++){
			if (!cicloHijo2.contains(h2.get(i))){
				h2.set(i,p1.get(i));
			}
		}
		
		hijo1.copiaGenes(h1.toArray());
		hijo2.copiaGenes(h2.toArray());
		hijo1.evalua();
		hijo2.evalua();
	
	}

	private ArrayList<Integer> encuentraCiclo(ArrayList<Integer> p1, ArrayList<Integer> p2) {
		ArrayList<Integer> ciclo = new ArrayList<Integer>();
		boolean encontrado=false;
		int init = p1.get(1);
		int indice =1;
		
		while (!encontrado){
			ciclo.add(p1.get(indice));
			if (p1.get(indice)==p2.get(indice)){
				ciclo.add(p1.get(indice));
				break;//si la correspondencia entre ciudades es igual, me salgo
			}
				
			if (init==p2.get(indice))
				encontrado=true;				
			
			indice = p1.indexOf(p2.get(indice));	//actualizo en indice
			}
		
		return ciclo;
	}

	public void cruceCodificacionOrdinal(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2) {
		
		ArrayList<Integer> listaH1 = new ArrayList<Integer>();
		ArrayList<Integer> listaH2;
		
		ArrayList<Integer> codh1 = new ArrayList<Integer>();
		ArrayList<Integer> codh2 = new ArrayList<Integer>();
		
		ArrayList<Integer> h1 = new ArrayList<Integer>();
		ArrayList<Integer> h2 = new ArrayList<Integer>();
		
		ArrayList<Integer> p1Genes = toArrayList(padre1.genes);
		ArrayList<Integer> p2Genes = toArrayList(padre2.genes);
		///ArrayList<Integer> p1Genes = new  ArrayList<Integer>();
		//ArrayList<Integer> p2Genes = new  ArrayList<Integer>();
		
		/*p1Genes.add(0);
		p1Genes.add(2);
		p1Genes.add(5);
		p1Genes.add(3);
		p1Genes.add(1);
		p1Genes.add(4);
		p1Genes.add(7);
		p1Genes.add(6);
		
		p2Genes.add(1);
		p2Genes.add(6);
		p2Genes.add(7);
		p2Genes.add(0);
		p2Genes.add(3);
		p2Genes.add(4);
		p2Genes.add(2);
		p2Genes.add(5);*/
		
		
		
		//inicializamos las listas
		
		for (int i=0;i<padre1.longitudTotal;i++){
			listaH1.add(i);
			codh1.add(i, -1);
			codh2.add(i, -1);
		}		
		listaH2 = (ArrayList<Integer>) listaH1.clone();
		 
		//recorremos el padre 1 y vamos rellenando el hijo, esta es la fase de codficacion
		
		for (int i=0;i<padre1.longitudTotal;i++){
			
			int elem = p1Genes.get(i);
			int indice = listaH1.indexOf(elem);
			listaH1.remove(indice);
			codh1.set(i, indice);
			
					
		}
		//recorremos el padre 2 y vamos rellenando el hijo, esta es la fase de codficacion
		for (int i=0;i<padre1.longitudTotal;i++){			
			int elem = p2Genes.get(i);
			int indice = listaH2.indexOf(elem);
			listaH2.remove(indice);
			codh2.set(i, indice);			
					
		}
		
		//aplicamos cruce clásico
		
		int puntoDeCorte1 = randomWithRange(3,p1Genes.size()-2);	
		//int puntoDeCorte1 = 3;
		for ( int i=puntoDeCorte1;i<padre1.longitudTotal;i++){	
			int aux = codh1.get(i);
			codh1.set(i, codh2.get(i));
			codh2.set(i, aux);			
		}
		
		
		listaH1 = new ArrayList<Integer>();
		for (int i=0;i<padre1.longitudTotal;i++){
			listaH1.add(i);
		}		
		listaH2 = (ArrayList<Integer>) listaH1.clone();
		
		
		 //aplicamos decodificacion tanto a h1 como a h2
		for (int i=0;i< padre1.longitudTotal;i++){
			int pos = codh1.get(i); 
			int elem = listaH1.get(pos);
			int indiceElem = listaH1.indexOf(elem);
			h1.add(elem);
			listaH1.remove(indiceElem);
		}
		
		for (int i=0;i< padre1.longitudTotal;i++){
			int pos = codh2.get(i); 
			int elem = listaH2.get(pos);
			int indiceElem = listaH2.indexOf(elem);
			h2.add(elem);
			listaH2.remove(indiceElem);
		}
		
		hijo1.copiaGenes(h1.toArray());
		hijo2.copiaGenes(h2.toArray());
		hijo1.evalua();
		hijo2.evalua();
		
		 
		
	}

	public void cruceOXPP(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2) {
		/*
		 * 1- Elegir aleatoriamente n posiciones de los padres.
		 * 2- Intercambiarlas en los hijos que se generan.
		 * 3- Para los valores que faltan en los hijos se copian los valores de los padres por orden a partir del segundo corte.
		 *     Si el valor ya est� en la cadena se omite y se pasa al siguiente. Cuando ya se han puesto todos los posibles
		 *     se rellenan los huecos con los del padre en orden.
		 */
	//-------------------------PASO 1-------------------------------------------//	
		//Elegir aleatoriamente 8 posiciones (nOXPP)
		int[] posiciones = new int[8];
		boolean esta1[] = new boolean [28];
		int n = 0;
		for(int i=0; i<nOXPP; i++){
			while(esta1[n]){
				n = (int) ((Math.random() * 28));
			}
			posiciones[i]=n;
			esta1[n]=true;			
		}
		
	
		esta1 = new boolean [28]; //arrays booleanos para tener en cuenta las ciudades que ya estan
		boolean esta2[] = new boolean [28]; // a�adidas a cada hijo.
		//-------------------------PASO 2-------------------------------------------//	
		//Intercambiar las posicones prioritarias en los hijos que se generan
		
		hijo1 = new cromosoma();//inicializo el cromosoma, tendra valores en los genes pero se reemplazaran
		hijo2 = new cromosoma();
		
		for(int i=0; i<nOXPP; i++){
			hijo1.setGenEnPos(posiciones[i], padre2.getGenes(posiciones[i]));
			hijo2.setGenEnPos(posiciones[i], padre1.getGenes(posiciones[i]));
			esta1[padre2.getGenes(posiciones[i])]=true;
			esta2[padre1.getGenes(posiciones[i])]=true;			
		}
		
		//---------------------------PASO 3----------------------------------------//
		//Copiamos a partir de la ultima posicion intercambiada, los que no est�n aun en el hijo. Si est� el valor,
		// lo omitimos por el momento.
		int max=0;
		for(int i=0; i<nOXPP; i++){ //sacamos la posicion mayor, a partir de la cual vamos a ir rellenando.
			if(posiciones[i]>max)
				max = posiciones[i];
		}
		
		int longitud = padre1.getlongitudTotal();
		int j=0;
		
		for(int i=(max+1); i!=max; i++){ //empezamos en la posicion siguiente al mayor indice de PP 
			i=i%longitud;
				while(esta1[padre1.getGenes(j)]){ //Si el valor que vamos a meter ya esta en el hijo, cogemos el siguiente
					j=(j+1)%longitud;							//Si el contador j esta sobre una PP, pasamos a la siguiente.
				}
				hijo1.setGenEnPos(i, padre1.getGenes(i)); //colocamos el valor del padre en el hijo.
			j=(j+1)%longitud;
		}		
		
		
		j=0;
		for(int i=(max+1); i!=max; i++){ //empezamos en la posicion siguiente al mayor indice de PP
			i=i%longitud;
				while(esta2[padre2.getGenes(j)]){ //Si el valor que vamos a meter ya esta en el hijo, cogemos el siguiente
					j=(j+1)%longitud;							//Si el contador j esta sobre una PP, pasamos a la siguiente.
				}
				hijo2.setGenEnPos(i, padre2.getGenes(i)); //colocamos el valor del padre en el hijo.
			j=(j+1)%longitud;
		}				
	}
	
	private boolean esPP(int i, int[] posiciones){
		for(int j=0; j<posiciones.length; j++){
			if(posiciones[j]==i)
				return true;
		}
		return false;
	}
	
	public void cruceOX(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2) {
		/*
		 * 1- Elegir aleatoriamente dos puntos de corte
		 * 2- Intercambiar las dos subcadenas comprendidas entre dichos puntos de corte en los hijos que se generan
		 * 3- Para los valores que faltan en los hijos se copian los valores de los padres por orden a partir del segundo corte.
		 *     Si el valor ya est� en la cadena se omite y se pasa al siguiente. Cuando ya se han puesto todos los posibles
		 *     se rellenan los huecos con los del padre en orden.
		 */
		int longitud = padre1.getlongitudTotal();
	//-------------------------PASO 1-------------------------------------------//	
		//Elegir aleatoriamente dos puntos de corte
		int puntoDeCorte1 = randomWithRange(1,longitud-1);
		int puntoDeCorte2 = randomWithRange(1,longitud-1);
		
		while (puntoDeCorte1==puntoDeCorte2){
			 puntoDeCorte1 = randomWithRange(1,longitud-1);
			 puntoDeCorte2 = randomWithRange(1,longitud-1);
		}
		
		//Nos aseguramos que los puntos de corte son disitntos
		
		
		if (puntoDeCorte2 < puntoDeCorte1){//si punto de corte 2 es menor que el punto de corte 1, los intercambio
			int aux = puntoDeCorte1;
			puntoDeCorte1 = puntoDeCorte2;
			puntoDeCorte2 = aux;
		}
		
		boolean esta1[] = new boolean [28];
		boolean esta2[] = new boolean [28];
		//-------------------------PASO 2-------------------------------------------//	
		//Intercambiar las dos subcadenas comprendidas entre dichos puntos de corte en los hijos que se generan
		
		hijo1 = new cromosoma();//inicializo el cromosoma.
		hijo2 = new cromosoma();
		int[] subcanedaInicial1 = new int[puntoDeCorte2-puntoDeCorte1+1];
		int[] subcanedaInicial2 = new int[puntoDeCorte2-puntoDeCorte1+1];
		for (int i=puntoDeCorte1; i<=puntoDeCorte2;i++){//rellenamos el intervalo de los dos puntos de corte
			hijo1.setGenEnPos(i, padre2.getGenes(i));
			hijo2.setGenEnPos(i, padre1.getGenes(i));
			subcanedaInicial1[i-puntoDeCorte1]= padre2.getGenes(i);
			subcanedaInicial2[i-puntoDeCorte1]= padre1.getGenes(i);
			esta1[padre2.getGenes(i)]=true;
			esta2[padre1.getGenes(i)]=true;
		}
		//---------------------------PASO 3----------------------------------------//
		//Copiamos a partir del punto de corte 2, los que no est�n aun en el hijo. Si est� el valor, lo omitimos por
		//el momento.
		
		//hijo1
		int j=puntoDeCorte2 + 1;  //j=posicion padre; i=posicion hijo
		for(int i=puntoDeCorte2+1; i<longitud; i++){
			if(!esta1[padre1.getGenes(j%longitud)]){ //si no est� el del padre en el hijo, lo ponemos.
				hijo1.setGenEnPos(i, padre1.getGenes(j%longitud));
				esta1[padre1.getGenes(j%longitud)]=true;
			}else{
				j = j%longitud + 1; //Si ya esta el del padre, pasamos al siguiente del padre
			}				
			j = j%longitud + 1;
		}
		for(int i=0; i<puntoDeCorte1; i++){
			if(!esta1[padre1.getGenes(j%longitud)]){
				hijo1.setGenEnPos(i, padre1.getGenes(j%longitud));
				esta1[padre1.getGenes(j%longitud)]=true;
			}else{
				j = j%longitud + 1;
			}
			j = j%longitud + 1;
		}
		
		//hijo2
		j=puntoDeCorte2 + 1;  //j=posicion padre; i=posicion hijo
		for(int i=puntoDeCorte2+1; i<longitud; i++){
			if(!esta2[padre2.getGenes(j%longitud)]){ //si no est� el del padre en el hijo, lo ponemos.
				hijo2.setGenEnPos(i, padre2.getGenes(j%longitud));
				esta2[padre2.getGenes(j%longitud)]=true;
			}else{
				j = j%longitud + 1; //Si ya est� el del padre, pasamos al siguiente del padre
			}				
			j = j%longitud + 1;
		}
		for(int i=0; i<puntoDeCorte1; i++){
			if(!esta2[padre2.getGenes(j%longitud)]){
				hijo2.setGenEnPos(i, padre2.getGenes(j%longitud));
				esta2[padre2.getGenes(j%longitud)]=true;
			}else{
				j = j%longitud + 1;
			}
			j = j%longitud + 1;
		}
		
	}
	
	public void crucePMX(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2) {
		/*
		 * 1- Elegir aleatoriamente dos puntos de corte
		 * 2- Intercambiar las dos subcadenas comprendidas entre dichos puntos de corte en los hijos que se generan
		 * 3- Para los valores que faltan en los hijos se copian los valores de los padres
		 * 		a- si un valor no esta en la subcadena intercambiada, se copia igual
		 * 		b- si está en la subacadena intercambiada, entonces se sustituye por el valor que tenga dicha cadena en el otro padre
		 */
	//hijo1 = new cromosoma (padre1);
	//hijo2 = new cromosoma (padre2);
//-------------------------PASO 1-------------------------------------------//	
	//Elegir aleatoriamente dos puntos de corte
	int puntoDeCorte1 = randomWithRange(1,padre1.longitudTotal-1);
	int puntoDeCorte2 = randomWithRange(1,padre1.longitudTotal-1);
	
	while (puntoDeCorte1==puntoDeCorte2){
		 puntoDeCorte1 = randomWithRange(1,padre1.longitudTotal-1);
		 puntoDeCorte2 = randomWithRange(1,padre1.longitudTotal-1);
	}
	
	//Nos aseguramos que los puntos de corte son disitntos
	
	
	if (puntoDeCorte2 < puntoDeCorte1){//si punto de corte 2 es menor que el punto de corte 1, los intercambio
		int aux = puntoDeCorte1;
		puntoDeCorte1 = puntoDeCorte2;
		puntoDeCorte2 = aux;
	}
	
	//-------------------------PASO 2-------------------------------------------//	
	//Intercambiar las dos subcadenas comprendidas entre dichos puntos de corte en los hijos que se generan	

		int[] subcanedaInicial1 = new int[puntoDeCorte2-puntoDeCorte1 +1];
		int[] subcanedaInicial2 = new int[puntoDeCorte2-puntoDeCorte1 +1];
		ArrayList<Integer> subcanedaInicial1al = new ArrayList<Integer>();
		ArrayList<Integer> subcanedaInicial2al = new ArrayList<Integer>();
		for (int i=puntoDeCorte1; i<=puntoDeCorte2;i++){//rellenamos el intervalo de los dos puntos de corte
			hijo2.setGenEnPos(i, padre1.getGenes(i));
			hijo1.setGenEnPos(i, padre2.getGenes(i));
			subcanedaInicial1[i-puntoDeCorte1]= padre2.getGenes(i);
			subcanedaInicial2[i-puntoDeCorte1]= padre1.getGenes(i);
			subcanedaInicial1al.add(padre2.getGenes(i));
			subcanedaInicial2al.add(padre1.getGenes(i));
		}

		//-------------------------PASO 3  a-------------------------------------------//	
		//primero, obtenemos las partes de los padres que ya no estan en los hijos, es decir desde 1 hasta puntoDeCorte1-1 y desde puntoDeCorte2+1 hasta longitudTotal-1
		int [] parteIzquierdaPadre1 = new int[puntoDeCorte1];
		int [] parteIzquierdaPadre2=  new int[puntoDeCorte1];
		int [] parteDerechaPadre1 = new int[padre1.longitudTotal-1 - puntoDeCorte2];
		int [] parteDerechaPadre2=  new int[padre1.longitudTotal-1 - puntoDeCorte2];
		
		ArrayList<Integer> parteIzquierdaPadre1al = new ArrayList<Integer>();
		ArrayList<Integer> parteIzquierdaPadre2al = new ArrayList<Integer>();
		ArrayList<Integer> parteDerechaPadre1al =  new ArrayList<Integer>();
		ArrayList<Integer> parteDerechaPadre2al =  new ArrayList<Integer>();
		
		
		for (int i=0;i<puntoDeCorte1;i++){
			parteIzquierdaPadre1[i]=padre1.getGenes(i);
			parteIzquierdaPadre2[i]=padre2.getGenes(i);
			parteIzquierdaPadre1al.add(padre1.getGenes(i));
			parteIzquierdaPadre2al.add(padre2.getGenes(i));
		}
		for (int i=puntoDeCorte2+1;i<padre1.longitudTotal;i++){
			int indice = i - (puntoDeCorte2+1);
			parteDerechaPadre1[indice]=padre1.getGenes(i);
			parteDerechaPadre2[indice]=padre2.getGenes(i);
			parteDerechaPadre1al.add(padre1.getGenes(i));
			parteDerechaPadre2al.add(padre2.getGenes(i));
		}
		
		
		for (int i=puntoDeCorte1;i<=puntoDeCorte2;i++){
			
			int parteIzH1 = parteIzquierdaPadre1al.indexOf(subcanedaInicial1al.get(i-puntoDeCorte1));
			if (parteIzH1!=-1){
				
				int aux = correspondencia(i-puntoDeCorte1,subcanedaInicial1al,subcanedaInicial2al);				
				//int aux=subcanedaInicial2al.get(i-puntoDeCorte1);
				hijo1.setGenEnPos(parteIzH1, aux);
				parteIzquierdaPadre1al.set(parteIzH1, aux);//actualizo
				
			}
			int parteIzH2 = parteIzquierdaPadre2al.indexOf(subcanedaInicial2al.get(i-puntoDeCorte1));
			if (parteIzH2!=-1){
				
				int aux = correspondencia(i-puntoDeCorte1,subcanedaInicial2al,subcanedaInicial1al);
				//int aux=subcanedaInicial1al.get(i-puntoDeCorte1);
				hijo2.setGenEnPos(parteIzH2, aux);
				parteIzquierdaPadre2al.set(parteIzH2, aux);//actualizo
			}
			
			int parteDerH1 = parteDerechaPadre1al.indexOf(subcanedaInicial1al.get(i-puntoDeCorte1));
			
			if (parteDerH1!=-1){
				
				int aux = correspondencia(i-puntoDeCorte1,subcanedaInicial1al,subcanedaInicial2al);
				
				//int aux=subcanedaInicial2al.get(i-puntoDeCorte1);
				hijo1.setGenEnPos(parteDerH1+ puntoDeCorte2 + 1 , aux);
				parteDerechaPadre1al.set(parteDerH1, aux);
			}
			
			int parteDerH2 = parteDerechaPadre2al.indexOf(subcanedaInicial2al.get(i-puntoDeCorte1));
			if (parteDerH2!=-1){
				
				int aux = correspondencia(i-puntoDeCorte1,subcanedaInicial2al,subcanedaInicial1al);
				
				//int aux=subcanedaInicial1al.get(i-puntoDeCorte1);
				hijo2.setGenEnPos(parteDerH2+ puntoDeCorte2 + 1, aux);
				parteDerechaPadre2al.set(parteDerH2, aux);
			}
			
		}			
		
	}
	private int correspondencia (int indice,ArrayList<Integer> A1, ArrayList<Integer> A2){
		
		int index = A1.indexOf(A2.get(indice));
		if (index!=-1){
			return correspondencia(index,A1,A2);
		}	
		return A2.get(indice);
	}
	private ArrayList<Integer> toArrayList(int[] genes) {
		ArrayList<Integer> aux = new ArrayList<Integer>();
		for (int i=0;i<genes.length;i++)
			aux.add(genes[i]);
		return aux;
	}
	int randomWithRange(int min, int max){
		   int range = (max - min) + 1;     
		   return (int)(Math.random() * range) + min;
		}
	
	
	
	
	
	
	
	
	
}
