package OperadoresGeneticos;

import java.util.ArrayList;

import logica.cromosoma;

public class MetodosMutacion {


	public MetodosMutacion(){
		
	}

	public cromosoma[] heuristica(int ciudades, int tam_pob, cromosoma[] pob, double prob_mut) {
		for (int i=0;i<tam_pob;i++){
			double r = Math.random();
			if (r<1){
				int pos;
				ArrayList<Integer> ciudadesAPermutar = new ArrayList<>();
				ArrayList<Integer> indicesOriginales = new ArrayList<>();
				ArrayList<Integer> aux = toArrayList(pob[i].genes);
				for (int j=0;j<ciudades;j++){ //Añado siempre ciudades distintas
					pos = randomWithRange(0,pob[i].longitudTotal-1);	
					while (ciudadesAPermutar.contains(pos)){
						pos = randomWithRange(0,pob[i].longitudTotal-1);	
					}
					ciudadesAPermutar.add(pos);
				}
				for (int j=0;j<ciudadesAPermutar.size();j++){
					indicesOriginales.add(aux.indexOf(ciudadesAPermutar.get(j)));
				}


				ArrayList<ArrayList<Integer>> permutaciones = new ArrayList<ArrayList<Integer>>();
				permutar(ciudadesAPermutar,ciudadesAPermutar,0,permutaciones);

				double mejorAptitud = Double.MAX_VALUE;
				cromosoma hijoMutado;
				cromosoma mejorHijoMutado = null;

				for (ArrayList<Integer> posicion: permutaciones){
					hijoMutado = creaCromosoma(aux,indicesOriginales,posicion);			 
					double fitness = hijoMutado.getAptitud();
					if (fitness<mejorAptitud){
						mejorAptitud=fitness;
						mejorHijoMutado = new cromosoma (hijoMutado);				 
					}	
				}

				pob[i] = new cromosoma(mejorHijoMutado);
				compruebaHijo(pob[i]); // 
			}

		}
		return pob;

	}


	public cromosoma[] inversion(int tam_pob, cromosoma[] pob, double prob_mut) {
		/*En este tipo de mutación se altera el orden de una subcadena o tramo del individuo, 
		 * Se aplica con una determinada probabilidad y consiste en seleccionar dos puntos
		 * del individuo al azar e invertir los elementos que hay entre dichos puntos
		 * 
		 * */
		for (int i=0;i<tam_pob;i++){
			double r = Math.random();
			if (r<prob_mut){

				int posicion1 = randomWithRange(0,pob[i].longitudTotal-1);	
				int posicion2 = randomWithRange(0,pob[i].longitudTotal-1);	
				while (posicion1==posicion2){ // me aseguroo que de que las dos posiciones son disitntas
					posicion1 = randomWithRange(0,pob[i].longitudTotal-1);	
					posicion2 = randomWithRange(0,pob[i].longitudTotal-1);
				}

				if (posicion2 < posicion1){//si punto de corte 2 es menor que el punto de corte 1, los intercambio
					int aux = posicion1;
					posicion1 = posicion2;
					posicion2 = aux;
				}
				ArrayList<Integer> aux = toArrayList(pob[i].genes);
				ArrayList<Integer> subCadena = new ArrayList<Integer>();
				for (int j=posicion1;j<posicion2;j++){
					subCadena.add(aux.get(j));
					//aux.remove(j);
				}
				for (int j=posicion1;j<posicion2;j++){
					aux.set(j, subCadena.get(subCadena.size()-1));
					subCadena.remove(posicion2 - j - 1);
				}
				pob[i].copiaGenes(aux.toArray());	
				pob[i].evalua();

				compruebaHijo(pob[i]); // 
			}
		}
		return pob;
	}

	public cromosoma[] intercambio(int tam_pob, cromosoma[] pob, double prob_mut) {

		for (int i=0;i<tam_pob;i++){
			double r = Math.random();
			if (r<prob_mut){ //Si se cumple esta condición, el iesimo individuo va a mutar

				int posicion1 = randomWithRange(0,pob[i].longitudTotal-1);	
				int posicion2 = randomWithRange(0,pob[i].longitudTotal-1);	
				while (posicion1==posicion2){ // me aseguroo que de que las doos posicioones son disitntas
					posicion1 = randomWithRange(0,pob[i].longitudTotal-1);	
					posicion2 = randomWithRange(0,pob[i].longitudTotal-1);
				}
				ArrayList<Integer> aux = toArrayList(pob[i].genes);
				int valor1 = aux.get(posicion1);
				int valor2 = aux.get(posicion2);
				aux.remove(posicion1);
				aux.add(posicion1, valor2);
				aux.remove(posicion2);
				aux.add(posicion2, valor1);
				pob[i].copiaGenes(aux.toArray());	
				pob[i].evalua();
			}
		}
		return pob;

	}

	public cromosoma[] insercion(int tam_pob, cromosoma[] pob, double prob_mut) {	

		for (int i=0;i<tam_pob;i++){
			double r = Math.random();
			if (r<prob_mut){ //Si se cumple esta condición, el iesimo individuo va a mutar
				int valor = randomWithRange(0,pob[i].longitudTotal-1);	
				int posicion = randomWithRange(0,pob[i].longitudTotal-1);			
				ArrayList<Integer> aux = toArrayList(pob[i].genes);
				aux.remove(aux.indexOf(valor));
				aux.add(posicion, valor);			
				pob[i].copiaGenes(aux.toArray());	
				pob[i].evalua();
			}
		}
		
		return pob;
	}
	private ArrayList<Integer> toArrayList(int[] genes) {
		ArrayList<Integer> aux = new ArrayList<Integer>();
		for (int i=0;i<genes.length;i++)
			aux.add(genes[i]);
		return aux;
	}

	int randomWithRange(int min, int max){
		int range = (max - min) + 1;     
		return (int)(Math.random() * range) + min;
	}
	private boolean compruebaHijo (cromosoma hijo){
		boolean correcto=true;

		for (int i=0;i<hijo.longitudTotal-1;i++)
			for(int j=i+1;j<hijo.longitudTotal;j++)
				if (hijo.getGenes(i)==hijo.getGenes(j)){
					return false;

				}

		return correcto;
	}
	private void permutar(ArrayList<Integer> cad, ArrayList<Integer>elemRest, int pos, ArrayList<ArrayList<Integer>> listPerm){	

		if(elemRest.size()==1){
			ArrayList<Integer> cadaux= (ArrayList<Integer>) cad.clone();
			cadaux.set(pos,elemRest.get(0));
			listPerm.add(cadaux);
		}
		else{
			for(int i=0; i<elemRest.size(); i++){
				ArrayList<Integer> elemRestaux = (ArrayList<Integer>) elemRest.clone();
				ArrayList<Integer> cadaux= (ArrayList<Integer>) cad.clone();
				cadaux.set(pos,elemRestaux.remove(i));
				permutar(cadaux,elemRestaux,pos+1,listPerm);
			}
		}
	}
	private cromosoma creaCromosoma(ArrayList<Integer> aux, ArrayList<Integer> indicesOriginales,ArrayList<Integer> posicion) {

		for (int i=0;i<indicesOriginales.size();i++){
			aux.set(indicesOriginales.get(i), posicion.get(i));
		}
		cromosoma p = new cromosoma();
		p.copiaGenes(aux.toArray());
		p.evalua();
		return p;	
	}



}
